<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login')); 
});

Auth::routes(['verify'=>true]);

Route::prefix('home')->middleware(['auth','verified'])->group(function () {
    // Buat route didalam ini, jangan diluar
    Route::prefix('staff')->middleware(['staff'])->group(function () { 
        // Untuk fitur staff, silahkan buat route disini
        Route::prefix('zoom')->group(function () {
            Route::get('/', [App\Http\Controllers\ZoomController::class, 'index'])->name('zoom.index');
            Route::get('create', [App\Http\Controllers\ZoomController::class, 'create'])->name('zoom.create');
        });
        
        Route::prefix('peminjaman')->group(function () {
            Route::get('/', [App\Http\Controllers\PeminjamanController::class, 'index'])->name('peminjaman.index');
            Route::get('create',[App\Http\Controllers\PeminjamanController::class,'create'])->name('peminjaman.create');
        });
    });
    
    Route::prefix('mahasiswa')->middleware(['mahasiswa'])->group(function () {
        // Untuk fitur mahasiswa, silahkan buat route disini
        Route::prefix('pengajuan-peminjaman')->group(function () {
            Route::get('/', [App\Http\Controllers\PengajuanPeminjamanController::class, 'index'])->name('pengajuan.index');
            Route::get('create', [App\Http\Controllers\PengajuanPeminjamanController::class, 'create'])->name('pengajuan.create');
        });
    });
    
    // Bisa diakses oleh semua, (staff dan mahasiswa)
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('jadwal-peminjaman', [App\Http\Controllers\JadwalController::class, 'index'])->name('jadwal.index');
});
