## Tahapan Instalasi
1. Clone repository ini, kemudian buka Visual Studio Code
2. Jalankan perintah `composer update`
3. Kemudian, buat database baru di `PhpMyAdmin`, lalu rename file `.env.example` ubah menjadi `.env` dan ganti bagian `DB_DATABASE` dengan database yang sudah dibuat di `PhpMyAdmin`
4. Kemudian jalankan perintah `php artisan migrate`
5. Setelah itu, jalankan perintah `php artisan serve` lalu buka aplikasi di browser

## Tahapan Akun
- Untuk register sebagai mahasiswa, silahkan pilih `register` lalu pada bagian `Login As` pilih mahasiswa
- Untuk register sebagai staff, silahkan pilih `register` lalu pada bagian `Login As` pilih staff
- Ketika registerasi, anda akan dikirimkan kode verifikasi ke alamat email yang dimasukkan. Pastikan alamat email yang digunakan adalah alamat email sebenarnya.
- Jika berhasil diverifikasi, maka selanjutnya anda akan diarahkan kehalaman beranda

## Fitur Yang Sudah Selesai
1. Laravel Auth (Register, Login, Forgot Password)
2. Middleware
3. Migration
4. Model
5. Membagi Controller dan Route

## NOTE:
1. Pada `route.php` sudah dilakukan filtering terhadap masing-masing route untuk masing-masing user beserta middlewarenya, mohon disesuaikan dengan instruksi yang ada
2. Controller sudah disesuaikan, Middleware sudah disesuaikan
3. Views sudah disesuaikan, namun untuk masing-masing halaman harap sesuaikan dengan kebutuhan