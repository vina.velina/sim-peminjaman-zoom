<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PeminjamanController extends Controller
{
    public function index()
    {
        return view('home.pages.peminjaman.index', ['id_page' => 'peminjaman']);
    }

    public function create()
    {
        return view('home.pages.peminjaman.create', ['id_page' => 'peminjaman']);
    }

}
