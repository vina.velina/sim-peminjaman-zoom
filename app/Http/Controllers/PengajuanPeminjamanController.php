<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PengajuanPeminjamanController extends Controller
{
     public function index()
    {
        return view('home.pages.pengajuan.index', ['id_page' => 'pengajuan']);
    }

    public function create()
    {
        return view('home.pages.pengajuan.create', ['id_page' => 'pengajuan']);
    }
}
