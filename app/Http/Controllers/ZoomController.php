<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ZoomController extends Controller
{
     public function index()
    {
        return view('home.pages.zoom.index', ['id_page' => 'zoom']);
    }

    public function create()
    {
        return view('home.pages.zoom.create', ['id_page' => 'zoom']);
    }

}
